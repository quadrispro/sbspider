# README #

This README documents whatever steps are necessary to get the spider up and running.

# Setup and usage #

You're encouraged to set up a new `virtualenv` before installing the spider:

```
$ virtualenv env
$ source env/bin/activate
```

The package could be installed with a simple command:

```
$ python setup.py install
```

Once the installation's completed, run the following to see the help screen:

```
$ spider.py --help
usage: spider.py [-h] [-u URL [URL ...]] [-d DOMAIN [DOMAIN ...]] [-D]
                 [-O FILE]

Scrape Sainsbury's grocery site's Ripe Fruits page and returns a JSON array of
all the products on the page.

optional arguments:
  -h, --help            show this help message and exit
  -u URL [URL ...], --start-url URL [URL ...]
                        URLs where the spider will start to crawl from
  -d DOMAIN [DOMAIN ...], --allow-domain DOMAIN [DOMAIN ...]
                        List of domains that the spider is allowed to crawl
  -D, --debug           Enable debug output
  -O FILE, --output-document FILE
                        Write output to FILE instead of stdout

```

Once the installation's completed, one can run the spider as in the following
example:

```
$ spider.py \
 -u http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html \
 -d s3-website-eu-west-1.amazonaws.com
```

# Run test suite #

```
$ source env/bin/activate
$ python setup test
```
