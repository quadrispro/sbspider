from setuptools import setup

setup(
    name="sbspider",
    version="0.0.1",
    author="Alessio Treglia",
    author_email="quadrispro@ubuntu.com",
    install_requires=[
        "Scrapy>=1.1.0",
        "pyasn1>=0.1.9",
    ],
    scripts=["spider.py"],
    test_suite="tests",
)
