#!/usr/bin/env python

from __future__ import print_function
from decimal import Decimal
import argparse
import os
import sys

from scrapy import signals
from scrapy.crawler import CrawlerProcess
from scrapy.exporters import JsonLinesItemExporter
import scrapy

PROGNAME = os.path.basename(sys.argv[0])

__doc__ = """Scrape Sainsbury's grocery site's Ripe Fruits page and
returns a JSON array of all the products on the page."""


def quantize_decimal(value):
    return Decimal(value).quantize(Decimal('.00'))


def decimal_strip_currency_sign(value):
    return float(Decimal(value[1:]))


class ProductExporter(JsonLinesItemExporter):
    def __init__(self, *args, **kwargs):
        super(ProductExporter, self).__init__(*args, **kwargs)
        self.first_item = True
        self.nitems = 0
        self.total = 0

    def start_exporting(self):
        self.file.write('{"results":[')

    def finish_exporting(self):
        self.file.write('],"total":{}}}'.format(
            quantize_decimal(self.total)
        ))

    def export_item(self, item):
        self.total += quantize_decimal(
            decimal_strip_currency_sign(item['unit_price']),
        )
        if self.first_item:
            self.first_item = False
        else:
            self.file.write(',')
        itemdict = dict(self._get_serialized_fields(item))
        self.file.write(self.encoder.encode(itemdict))


class ProductExporterPipeline(object):

    default_file = sys.stdout

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def get_output_file(self):
        return getattr(self, 'file', self.default_file)

    def spider_opened(self, spider):
        self.file = self.get_output_file()
        self.exporter = ProductExporter(self.file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class SainsburyItem(scrapy.Item):
    title = scrapy.Field()
    size = scrapy.Field()
    unit_price = scrapy.Field(serializer=decimal_strip_currency_sign)
    description = scrapy.Field()


class SBProductSpider(scrapy.Spider):
    name = "sbproduct"

    def parse(self, response):
        for href in response.xpath('//div[@class="productInfo"]/h3/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_item)

    def parse_item(self, response):
        meta = response.xpath('/html/head/meta[@property="og:title"]')
        title = meta.xpath('@content').extract_first()
        desc_div = meta.xpath(
            '//h3[text()="Description"]/following-sibling::div',
        )
        description = desc_div.xpath('p/text()').extract_first()
        ppu = response.xpath(
            '//p[@class="pricePerMeasure"]/text()',
        ).extract_first()
        item = SainsburyItem()
        item['title'] = title
        item['size'] = "{}kb".format(len(response.text) / 1024)
        item['description'] = description
        item['unit_price'] = ppu
        yield item


def crawl(settings, start_urls, allowed_domains, output_document):
    """Set up the crawlers as per client's parameters.
    """
    settings.update({
        'ITEM_PIPELINES': {'__main__.ProductExporterPipeline': 800},
    })

    process = CrawlerProcess(settings=settings)
    SBProductSpider.start_urls = start_urls
    SBProductSpider.allowed_domains = allowed_domains
    ProductExporterPipeline.file = output_document
    process.crawl(SBProductSpider)
    process.start()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog=PROGNAME,
        description=__doc__,
    )
    parser.add_argument(
        "-u", "--start-url", dest="start_urls", nargs="+",
        help="URLs where the spider will start to crawl from",
        metavar="URL",
    )
    parser.add_argument(
        "-d", "--allow-domain", dest="allowed_domains", nargs="+",
        help="List of domains that the spider is allowed to crawl",
        metavar="DOMAIN",
    )
    parser.add_argument(
        "-D", "--debug", dest="debug", action="store_true", default=False,
        help="Enable debug output",
    )
    parser.add_argument(
        "-O", "--output-document",
        dest="output_document",
        type=argparse.FileType("w"),
        default=ProductExporterPipeline.default_file,
        metavar="FILE",
        help="Write output to FILE instead of stdout",
    )
    args = parser.parse_args()
    sys.exit(
        crawl(
            {"LOG_ENABLED": args.debug},
            args.start_urls,
            args.allowed_domains,
            args.output_document,
        ),
    )
