from decimal import Decimal
from StringIO import StringIO
from unittest import TestCase
import json

from scrapy.crawler import CrawlerProcess

import spider


class WebsiteStandardTest(TestCase):

    @classmethod
    def setUpClass(self):
        self.domain = "hiring-tests.s3-website-eu-west-1.amazonaws.com"
        self.url = ("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/"
                    "2015_Developer_Scrape/5_products.html")

        # Mock file
        def _fake_close(*args, **kwargs):
            pass
        output_document = StringIO()
        output_document_old_close = output_document.close
        output_document.close = _fake_close

        settings = {
            'ITEM_PIPELINES': {'spider.ProductExporterPipeline': 800},
            'LOG_ENABLED': False,
        }
        process = CrawlerProcess(settings)
        spider.SBProductSpider.start_urls = [self.url]
        spider.SBProductSpider.allowed_domains = [self.domain]
        spider.ProductExporterPipeline.file = output_document
        process.crawl(spider.SBProductSpider)
        process.start()
        output_document.seek(0)
        self.json_doc = json.loads(output_document.read())
        # Actually close the file to prevent memory leak
        output_document.close = output_document_old_close
        output_document.close()

    def test_nitems(self):
        self.assertEqual(len(self.json_doc['results']), 7)

    def test_total(self):
        self.assertEqual(self.json_doc['total'], 9.6)

    def test_item_in_results(self):
        item = {
            'size': '38kb',
            'description': 'Kiwi',
            'unit_price': 0.45,
            'title': "Sainsbury's Kiwi Fruit, Ripe & Ready x4",
        }
        self.assertIn(item, self.json_doc['results'])


class AuxTestCases(TestCase):

    def test_decimal_strip_currency_sign(self):
        self.assertEqual(
            float("1.67835555555"),
            spider.decimal_strip_currency_sign("$1.67835555555"),
        )

    def test_quantize_decimal(self):
        value = "1.67835555555"
        self.assertEqual(
            Decimal("1.68"),
            spider.quantize_decimal(value),
        )
